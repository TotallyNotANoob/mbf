﻿CREATE
DEFINER = 'root'@'localhost'
VIEW mbf.task3
AS
SELECT
  `users`.`name` AS `user name`,
  `communities`.`name` AS `community name`,
  `permissions`.`name` AS `permission name`
FROM ((((`users`
  JOIN `community_members`
    ON ((`users`.`id` = `community_members`.`user_id`)))
  JOIN `communities`
    ON ((`communities`.`id` = `community_members`.`community_id`)))
  JOIN `community_member_permissions`
    ON ((`community_member_permissions`.`member_id` = `users`.`id`)))
  JOIN `permissions`
    ON ((`community_member_permissions`.`permission_id` = `permissions`.`id`)))
WHERE (((LOWER(`users`.`name`) LIKE '%t%')
OR (`permissions`.`name` LIKE '%articles%'))
AND (LENGTH(`communities`.`name`) >= 15))
ORDER BY `users`.`name`;