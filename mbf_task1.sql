﻿CREATE
DEFINER = 'root'@'localhost'
VIEW mbf.task1
AS
SELECT
  users.name AS `user bame`,
  communities.name AS `community name`,
  community_members.joined_at AS `joined at`
FROM users
  INNER JOIN community_members
    ON users.id = community_members.user_id
  INNER JOIN communities
    ON communities.id = community_members.community_id
WHERE community_members.joined_at >= '2013-01-01'
ORDER BY community_members.joined_at DESC;