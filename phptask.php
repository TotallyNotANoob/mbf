<html>
  <body>
  <?php
function highlight_nicknames(string $text): string
{
  preg_match_all("/(^|\s)@[a-zA-Z][a-zA-Z0-9]*($|\s)/", $text,$m_array);
	foreach ($m_array[0] as $nickname){
    $text = str_replace($nickname, "<b>".$nickname."</b>", $text);
  }
  return $text;
}
 
 if (isset($_GET['inpstring'])){
	echo "original string:<br>".$_GET['inpstring']."<br><br>";
	echo "modified string:<br>".highlight_nicknames($_GET['inpstring'])."<br><br>";
 }
 
?>
<form action="phptask.php" method="get">
  Enter string: <input type="text" name="inpstring"><br>
  <button type="submit">Submit</button>
</form>
  </body>
</html>