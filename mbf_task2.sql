﻿CREATE
DEFINER = 'root'@'localhost'
VIEW mbf.task2
AS
SELECT
  `communities`.`id` AS `community id`,
  `permissions`.`name` AS `permission name`,
  COUNT(`community_member_permissions`.`member_id`) AS `privileged users count`
FROM (((`communities`
  JOIN `community_members`
    ON ((`communities`.`id` = `community_members`.`community_id`)))
  JOIN `community_member_permissions`
    ON ((`community_members`.`user_id` = `community_member_permissions`.`member_id`)))
  JOIN `permissions`
    ON ((`permissions`.`id` = `community_member_permissions`.`permission_id`)))
GROUP BY `communities`.`id`,
         `permissions`.`name`
HAVING (COUNT(`community_member_permissions`.`member_id`) > 5)
ORDER BY `communities`.`id` DESC, `privileged users count` LIMIT 100;